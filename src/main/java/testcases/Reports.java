package testcases;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {
	public static ExtentTest test;
	public static ExtentReports extent;
	public static ExtentHtmlReporter html;
	public static String testCaseName;
	public static String testCaseDesc;
	public static String Author;
	public static String category;
	public static String FileName;
	
	@BeforeSuite
	public void startReports(){
		html=new ExtentHtmlReporter("./Reports/report.html");
		html.setAppendExisting(true);
		extent=new ExtentReports();
		extent.attachReporter(html);
	}
    @BeforeMethod
	public void startTestCase()
	{
		test=extent.createTest(testCaseName, testCaseDesc);
		test.assignAuthor(Author);
		test.assignCategory(category);
	}
    
    public void ReportStep(String desc,String status)
    {
    if(status.equalsIgnoreCase("passed"))
    {
    	test.pass(desc);
    }
    else
    {
    	test.fail(desc);
    }
    }
	//public static void main(String[] args) throws IOException {
	// TODO Auto-generated method stub
	//generates a html file in non editablemode
	//ExtentHtmlReporter html=new ExtentHtmlReporter("./Reports/report.html");
	//html.setAppendExisting(true);

	//to make it non editable [attach the html report]
	//ExtentReports extent=new ExtentReports();
	//extent.attachReporter(html);

	//Give the details of the test cast
		//Author and category
		//test.assignAuthor("Priya");
		//test.assignCategory("regression");
		//attach screenshots for each step from the snaps folder using MediaEnityBuilder class{end with build())
		//test.pass("User name is entered Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./..snaps/img1.png").build());

	@AfterSuite
	public void endReports(){
		extent.flush();
	}



}


